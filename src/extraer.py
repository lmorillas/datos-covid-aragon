import json
import pandas as pd

'''
datos.txt -> xhr_streaming de https://datacovid.salud.aragon.es/covid/

https://github.com/websocket-client/websocket-client
https://stackoverflow.com/questions/63976759/get-universities-names-from-openstreetmap


'''


lineas = open('datos.txt').readlines()

# [l for l in lineas if 'recalculating' in l]

infoxhr = []
recs =[l for l in lineas if 'recalculated' in l]
for r in recs:
    texto = f'"{r[10:-2]}'
    x = json.loads(texto)
    if isinstance(x, str):
        x = json.loads(x)
    nombre = x.get('recalculating', {}).get('name')
    infoxhr.append(nombre)


r = [x for x in lineas][-1]
texto = f'"{r[10:-2]}'

json.loads(texto)

data = json.loads(texto)

if isinstance(data, str):
    data = json.loads(data)


def generar_datos(data):
    #resultado = [{'dia':d.get('x'), d.get('name'):d.get('y')} for d in data]
    
    df = pd.DataFrame()
    
    for el in data:
        for k in el.keys():
            if k not in df.columns:
                try:
                    df[k] = el[k]
                except:
                    print(k)
    return df


datos = {}
claves = data['values'].keys()
for c in claves:
    if c in ['AmbitoCur', 'EdadCur']:
        datos_ = [{'dia':d.get('x'), d.get('name'):d.get('y')} 
                for d in data['values'][c]['x']['data']]
        df = generar_datos(datos_)
        datos[c] = df
    
hojas = {'AmbitoCur': 'Casos por Ambito', 'EdadCur': 'IA7 por edad'}

for c in datos:
    df = datos[c]
    colsexo = [c for c in  df.columns if 'Mujeres' in c ]
    if len(colsexo):
        sexo = ' (por sexo)'
    else:
        sexo = ''
    df.to_excel(f'{hojas[c]}{sexo}.xlsx', index=False)


from matplotlib import pyplot as plt
pd.options.plotting.backend = 'matplotlib'
plt.rcParams["figure.figsize"] = (12, 6)
ini = pd.to_datetime('2021-05-01')

df = datos.get('EdadCur')
if isinstance(df, pd.core.frame.DataFrame):
    if not colsexo:
        df.dia = pd.to_datetime(df.dia)
        #df = df[['dia','0-4 años','05-09 años', '10-14 años', '15-19 años']]
        df.set_index('dia', inplace=True)
        df.plot(title='IA7 por edad\nFuente: https://datacovid.salud.aragon.es/covid/',  xlabel = 'Fecha notificación', ylabel='Casos')
        plt.savefig('ia7.png')
        plt.close()

        dfabr = df[df.index >= ini]
        dfabr.plot(title='IA7 por edad (Mayo)\nFuente: https://datacovid.salud.aragon.es/covid/',  xlabel = 'Fecha notificación', ylabel='Casos')
        plt.savefig('ia7_mayo.png')
        plt.close()

        pd.options.plotting.backend = "plotly"
        fig = df.plot(title="IA7/100000 por edades\n(Fuente: https://datacovid.salud.aragon.es/covid/)",
                template="simple_white",
                labels=dict(index="Fecha", value="IA7/100000", variable="Franja edad"))
        fig.write_html('ia7_edades.html',include_plotlyjs='cdn')
        pd.options.plotting.backend = 'matplotlib'
    else:
        df.dia = pd.to_datetime(df.dia)
        df.set_index('dia', inplace=True)
        c90 = [c for c in df.columns if '90' in c]
        df90 = df[c90]
        pd.options.plotting.backend = "plotly"
        fig = df90.plot(title="IA7/100000 Más de 90 por sexo\n(Fuente: https://datacovid.salud.aragon.es/covid/)",
                template="simple_white",
                labels=dict(index="Fecha", value="IA7/100000", variable="Franja edad"))
        fig.write_html('ia7_90.html',include_plotlyjs='cdn')
        fig = df.plot(title="IA7/100000 por edad y sexo\n(Fuente: https://datacovid.salud.aragon.es/covid/)",
                template="simple_white",
                labels=dict(index="Fecha", value="IA7/100000", variable="Franja edad/sexo"))
        fig.write_html('ia7_edad_sexo.html',include_plotlyjs='cdn')
        pd.options.plotting.backend = 'matplotlib'

df = datos.get('AmbitoCur')
if isinstance(df, pd.core.frame.DataFrame):
    df.dia = pd.to_datetime(df.dia)
    #df.dia = pd.to_datetime(df.dia)
    df.set_index('dia', inplace=True)
    df.plot(title="Casos por ámbito\nFuente: https://datacovid.salud.aragon.es/covid/", xlabel = 'Fecha notificación', ylabel='Casos')
    plt.savefig('casos_ambito.png')
    plt.close()


    dfabr = df[df.index >= ini]
    dfabr.plot(title='Casos por ámbito (Mayo)\nFuente: https://datacovid.salud.aragon.es/covid/',  xlabel = 'Fecha notificación', ylabel='Casos')
    plt.savefig('casos_ambito_mayo.png')
    plt.close()

    pd.options.plotting.backend = "plotly"
    fig = df.plot(title="Casos por ámbito\n(Fuente: https://datacovid.salud.aragon.es/covid/)",
                template="simple_white",
                labels=dict(index="Fecha", value="Casos", variable="Ámbito de contagio"))
    fig.write_html('ambito.html',include_plotlyjs='cdn')

    pd.options.plotting.backend = 'matplotlib'


